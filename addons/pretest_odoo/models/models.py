# -*- coding: utf-8 -*-

from datetime import datetime
from odoo import models, fields, api


class Buku(models.Model):
    _name = 'list.book'
    _description = 'List Buku'

    name = fields.Char(string='Judul')
    category = fields.Selection([
        ('ops1', 'Umum'), 
        ('ops2', 'IT'), 
        ('ops3', 'Kesehatan'), 
        ('ops4', 'Politik')], 
        string='Kategori', required=True, help='Kategori Buku')
    book_date = fields.Date(string='Tanggal', help='Tanggal Terbit')
    partner_id = fields.Many2many('res.partner', string='Penulis')
    isbn = fields.Char(string='Kode ISBN', required=True)

    _sql_constraints = [
    ('isbn_unik', 'UNIQUE(isbn)', 'No ISBN harus unik'),
    ('isbn_cek', 'CHECK(isbn != name)', 'No ISBN tidak boleh diisi dengan Nama ')
    ]
    # new_isbn = record.isbn
    # isbn_records = env['list.book'].search([('isbn', '=', new_isbn)])
    # if len(isbn_records) > 1:
    #     raise Warning('Code ISBN sudah tersedia')

class Member(models.Model):
    _name = 'list.member'
    _description = 'List Member'

    name = fields.Char(string='Nama Member')
    no_card = fields.Char(string='Nomor Kartu Identitas')
    type_card= fields.Selection([
        ('id1', 'KTP'), 
        ('id2', 'SIM'), 
        ('id3', 'PASSPORT')], 
        string='Identitas', required=True, help='No Identitas')
    state = fields.Selection ([('state1', 'Draft'), ('state2', 'Approved')], string='Kewarganegaraan', required=True, help='Kewarganegaraan')

    _sql_constraints = [
    ('no_unik', 'UNIQUE(no_card)', 'No Identitas harus unik'),
    ('no_cek', 'CHECK(no_card != name)', 'No Identitas tidak boleh diisi dengan Nama ')
    ]

class TransaksiRental(models.Model):
    _name = 'transaksi.rental'
    _description = 'Transaksi Rental'
    
    @api.onchange('total_days', 'from_date', 'final_date')
    def calculate_date(self):
        if self.from_date and self.final_date:
            d1=datetime.strptime(str(self.from_date), '%Y-%m-%d')
            d2=datetime.strptime(str(self.final_date), '%Y-%m-%d')
            d3 = d2 - d1
            self.total_days=str(d3.days)

    @api.onchange('rent', 'total_days')
    def calculate_rent(self):
        self.rent = 1000 * self.total_days

    start_date = fields.Date(string='Tanggal Rental')
    user_id = fields.Many2one('list.member', string='Nama peminjam')
    listbook = fields.Many2one('list.book', string='Peminjaman Buku')
    from_date = fields.Date(string='Tanggal Pinjam')
    final_date = fields.Date(string='Tanggal Kembali')
    total_days = fields.Float(string='Durasi Pemnjaman')
    rent = fields.Float(string='Total Biaya Sewa')
   
    ref = fields.Char(string='No Transaksi', readonly=True, default='/')

    @api.model
    def create(self, vals):
        vals['ref'] = self.env['ir.sequence'].next_by_code('transaksi.rental')
        return super(TransaksiRental, self).create(vals)
